<nav class="navbar navbar-expand-sm bg-light">

  <!-- Links -->
  <ul class="nav nav-tabs mx-auto">
    <li class="nav-item">
        <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="/">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link  {{ Request::is('about') ? 'active' : '' }}" href="/about">about</a>
    </li>
    <li class="nav-item">
        <a class="nav-link  {{ Request::is('contact') ? 'active' : '' }}" href="/contact">contact</a>
    </li>
 </ul>

</nav>
@extends('blog')

@section('content')


<div class="container" style="min-height:400px;">
	<div class="row">

    <div class="col-4">
        <h2>Recent Post</h2>
        <ul class="nav flex-column">

            @foreach ($posts as $post)
            <li class="nav-item">
                <a class="nav-link" href="#">{{$post->title}}</a>
            </li>
            @endforeach

        </ul>
    </div>
	<div class="col-8">

		@foreach ($posts as $post)	
        <div class="post">
            <h3>{{$post->title}}</h3>
            <p>{{$post->body}}</p>
        </div>
        @endforeach
        {{ $posts->links() }}  
	</div>
	</div>
</div>

    
@endsection

@section('top-nav')

<nav class="navbar navbar-expand-sm bg-light">

  <!-- Links -->
  <ul class="navbar-nav mx-auto">
    <li class="nav-item">
      <a class="nav-link" href="/">HOME</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/about">ABOUT US</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/contact">CONTACT</a>
    </li>
  </ul>

</nav>

@endsection